import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the amount of the numbers: ");
        int amountOfNumbers = input.nextInt();
        int[] sequenceOfNumbers = new int[amountOfNumbers];
        System.out.println("Please, write each number individually by pressing Enter");
        int inputNumber;
        for (int i = 0; i < amountOfNumbers; i++){
            inputNumber = input.nextInt();
            sequenceOfNumbers[i] = inputNumber;
        }
        NumberFinder numberFinder = new NumberFinder(sequenceOfNumbers);
        numberFinder.showAllNumbers();
    }
}



