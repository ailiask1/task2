import java.util.Arrays;

public class NumberFinder {

    private int longestNumber;
    private int shortestNumber;
    private int numberWithMinimalDifferentDigits;
    private int[] sequenceOfNumbers;

    public NumberFinder(int[] sequenceOfNumbers) {
        this.sequenceOfNumbers = sequenceOfNumbers;
        this.longestNumber = sequenceOfNumbers[0];
        this.shortestNumber = sequenceOfNumbers[0];
        this.numberWithMinimalDifferentDigits = sequenceOfNumbers[0];
    }

    public int getNumberOfDigits(int number){
        return String.valueOf(number).length();
    }

    public int getNumberOfDifferentDigits(int number){
        char[] sortedNumber = ("" + number).toCharArray();
        Arrays.sort(sortedNumber);
        int differentDigits = 1;
        char currentDigit = sortedNumber[0];
        for (int i = 0; i < sortedNumber.length; i++){
            if (currentDigit != sortedNumber[i]){
                currentDigit = sortedNumber[i];
                differentDigits++;
            }
        }
        return differentDigits;
    }

    public boolean isIncreasingNumber(int number){
        int currentDigit = number % 10;
        number = number / 10;
        while (number > 0){
            if (currentDigit <= number % 10) {
                return false;
            }
            currentDigit = number % 10;
            number = number / 10;
        }
        return true;
    }

    public boolean hasDifferentDigits(int number){
        char[] sortedNumber = ("" + number).toCharArray();
        Arrays.sort(sortedNumber);
        for (int i = 0; i < sortedNumber.length; i++) {
            if (i+1 < sortedNumber.length) {
                if (sortedNumber[i] == sortedNumber[i+1]) {
                    return false;
                }
            }
        }
        return true;
    }

    public void findLongestNumber(){
        for (int i = 0; i < sequenceOfNumbers.length; i++){
            if (getNumberOfDigits(longestNumber) < getNumberOfDigits(sequenceOfNumbers[i])){
                longestNumber = sequenceOfNumbers[i];
            }
        }
        System.out.println("The longest number is " + longestNumber + " with length " + getNumberOfDigits(longestNumber));
    }

    public void findShortestNumber(){
        for (int i = 0; i < sequenceOfNumbers.length; i++){
            if (getNumberOfDigits(shortestNumber) > getNumberOfDigits(sequenceOfNumbers[i])){
                shortestNumber = sequenceOfNumbers[i];
            }
        }
        System.out.println("The shortest number is " + shortestNumber + " with length " + getNumberOfDigits(shortestNumber));
    }

    public void findNumberWithMinimalDifferentDigits(){
        for (int i = 0; i < sequenceOfNumbers.length; i++){
            if (getNumberOfDifferentDigits(numberWithMinimalDifferentDigits) > getNumberOfDifferentDigits(sequenceOfNumbers[i])){
                numberWithMinimalDifferentDigits = sequenceOfNumbers[i];
            }
        }
        System.out.println("The number with minimal different digits is " + numberWithMinimalDifferentDigits);
    }

    public void findNumberWithIncreasingDigits(){
        for (int i = 0; i < sequenceOfNumbers.length; i++){
            if (isIncreasingNumber(sequenceOfNumbers[i])){
                System.out.println("Number, the digits in which are in ascending order is " + sequenceOfNumbers[i]);
                return;
            }
        }
        System.out.println("Sorry, but there is no number with ascending order digits.");
    }

    public void findNumberWithDifferentDigits(){
        for (int i = 0; i < sequenceOfNumbers.length; i++){
            if (hasDifferentDigits(sequenceOfNumbers[i])){
                System.out.println("Number consisting only of various digits is " + sequenceOfNumbers[i]);
                return;
            }
        }
        System.out.println("Sorry, but there is no number with all different digits.");
    }

    public void showAllNumbers() {
        findLongestNumber();
        findShortestNumber();
        findNumberWithMinimalDifferentDigits();
        findNumberWithIncreasingDigits();
        findNumberWithDifferentDigits();
    }
}
